This project contains a function that allows the download and usage of an image as a texture.

Given the texture name and object to assign it to, it will check if the given texture (by name) exists, 
if not, download it from a url (server) and assign it to the object.