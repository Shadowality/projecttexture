﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class TextureManager : Singleton<TextureManager>
    {
        // Examples of objects that need textures.
        public GameObject background;

        public GameObject topLeftPlane;
        public GameObject topRightPlane;
        public GameObject bottomLeftPlane;
        public GameObject bottomRightPlane;

        // Examples of texture names.
        public string textureNameTest1 = "Test 1";

        public string textureNameTest2 = "Test 2";

        // A download progress bar.
        public Slider downloadBar;

        // Number of active threads, besides main.
        public int numThreads;

        // Dictionary with the textures' names and their respective URL link.
        public Dictionary<string, string> nomeURL = new Dictionary<string, string>();

        // Dictionary with the textures names and their bytes (image).
        public Dictionary<string, byte[]> nomeBytes = new Dictionary<string, byte[]>();

        // Active thread counter.
        public int activeThreads;

        // Example of urls of textures.
        private string url = "https://blogs.unity3d.com/wp-content/themes/unity/images/medium.jpg";

        private string url2 = "https://raw.githubusercontent.com/sschmid/Entitas-CSharp/master/Readme/Images/MadeForUnity.png";

        // Total amount of textures that need to be downloaded.
        private int numTexturesToDownload;

        // Folder used to store textures.
        private string textureFolderPath;

        /// <summary>
        /// Checks if the textures exist locally, if they do not - save them, and then load them.
        /// </summary>
        /// <returns></returns>
        public IEnumerator ProcessTextures()
        {
            string localPath;
            string textureNome;

            byte[] bytes = null;

            for (int i = 0; i < nomeURL.Count; i++)
            {
                textureNome = nomeURL.Keys.ElementAt(i);
                string textureNomeTemp = textureNome;

                Debug.Log("Checking if the texture " + textureNomeTemp + " exists locally...");

                localPath = Application.dataPath + "/../Assets/Textures/" + textureNomeTemp + ".png";

                //localPath = textureFolderPath + "/" + textureNome + ".png";

                if (!File.Exists(localPath))
                {
                    Debug.Log("Texture " + textureNomeTemp + " does not exist locally, starting download...");

                    numTexturesToDownload++;

                    WWW www = new WWW(nomeURL[textureNomeTemp]);

                    StartCoroutine(ShowDownloadProgress(www));

                    yield return www;

                    Debug.Log("Texture " + textureNomeTemp + " downloaded.");

                    numTexturesToDownload--;

                    bytes = www.bytes;

                    ThreadPool.QueueUserWorkItem(state => SaveAndLoad(localPath, textureNomeTemp, bytes));
                }
                else
                {
                    Debug.Log("Texture " + textureNomeTemp + " exists locally.");

                    ThreadPool.QueueUserWorkItem(state => Load(localPath, textureNomeTemp));
                }
            }

            // For testing purposes only. Assign textures to objects when all threads have finished processing.
            bool done = false;

            // (To be improved) Wait for all threads to be completed.
            Thread.Sleep(200);

            while (!done)
            {
                if (activeThreads == 0)
                {
                    done = true;
                    AssignTexture(background, nomeBytes[textureNameTest1]);
                    AssignTexture(topLeftPlane, nomeBytes[textureNameTest2]);
                    AssignTexture(topRightPlane, nomeBytes[textureNameTest2]);
                    AssignTexture(bottomLeftPlane, nomeBytes[textureNameTest2]);
                    AssignTexture(bottomRightPlane, nomeBytes[textureNameTest2]);
                }
            }
        }

        /// <summary>
        /// Assign to the gameobject a texture using the bytes given.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bytes"></param>
        public void AssignTexture(GameObject obj, byte[] bytes)
        {
            Texture2D texture = new Texture2D(4, 4);
            texture.LoadImage(bytes);

            Renderer renderer = obj.GetComponent<Renderer>();
            renderer.material.mainTexture = texture;

            Debug.Log("Assigning complete.");
        }

        /// <summary>
        /// Allow the same thread to load after saving.
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="textureName"></param>
        /// <param name="bytes"></param>
        private void SaveAndLoad(string localPath, string textureName, byte[] bytes)
        {
            Save(localPath, bytes, textureName);
            Load(localPath, textureName);
        }

        /// <summary>
        /// Save a texture locally.
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="bytes"></param>
        private void Save(string localPath, byte[] bytes, string textureName)
        {
            Interlocked.Increment(ref activeThreads);

            Debug.Log("Thread " + Thread.CurrentThread.ManagedThreadId + " starting texture " + textureName + " save.");

            File.WriteAllBytes(localPath, bytes);

            Debug.Log("Thread " + Thread.CurrentThread.ManagedThreadId + " finished texture " + textureName + " save.");

            Interlocked.Decrement(ref activeThreads);
        }

        /// <summary>
        /// Load a texture saved locally.
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="textureName"></param>
        private void Load(string localPath, string textureName)
        {
            Interlocked.Increment(ref activeThreads);

            Debug.Log("Thread " + Thread.CurrentThread.ManagedThreadId + " starting texture load" + " - " + textureName);

            byte[] bytes = File.ReadAllBytes(localPath);

            // Prevent simultaneous access to the dictionary.
            lock (nomeBytes)
                nomeBytes.Add(textureName, bytes);

            Debug.Log("Thread " + Thread.CurrentThread.ManagedThreadId + " finished texture load." + " - " + textureName);

            Interlocked.Decrement(ref activeThreads);
        }

        /// <summary>
        /// Show download progress.
        /// </summary>
        /// <param name="www"></param>
        /// <returns></returns>
        private IEnumerator ShowDownloadProgress(WWW www)
        {
            downloadBar.gameObject.SetActive(true);

            while (!www.isDone)
            {
                downloadBar.value = www.progress / numTexturesToDownload;

                //Debug.Log(www.progress);

                yield return new WaitForSeconds(0.1f);
            }

            downloadBar.gameObject.SetActive(false);
        }

        private void Start()
        {
            // Set the maximum amount of threads.
            ThreadPool.SetMaxThreads(numThreads, numThreads);

            // Create a folder to store textures.
            textureFolderPath = Application.persistentDataPath + "/Textures";

            if (!Directory.Exists(textureFolderPath))
                Directory.CreateDirectory(textureFolderPath);

            // Add texture names and respective URL to a dictionary.
            nomeURL.Add(textureNameTest1, url);
            nomeURL.Add(textureNameTest2, url2);

            // Process all textures within the dictionary.
            StartCoroutine(ProcessTextures());
        }
    }
}